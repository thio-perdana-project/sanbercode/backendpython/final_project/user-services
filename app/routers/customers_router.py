from app import app
from app.controllers import customers
from flask import Blueprint, request

customers_blueprint = Blueprint('customers_router', __name__)

@app.route("/users", methods=["GET"])
def showUsers():
    return customers.shows()

@app.route("/user", methods=["GET"])
def showUser():
    params = request.json
    return customers.show(**params)

@app.route("/user/insert", methods=["POST"])
def insertUser():
    params = request.json
    return customers.insert(**params)

@app.route("/user/update", methods=["POST"])
def updateUser():
    params = request.json
    return customers.update(**params)

@app.route("/user/delete", methods=["POST"])
def deleteUser():
    params = request.json
    return customers.delete(**params)

@app.route("/user/requesttoken", methods=["GET"])
def requestToken():
    params = request.json
    return customers.token(**params)